#include "jsonparser.h"
#include <QDebug>

JsonParser::JsonParser()
{
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
}

QString JsonParser::getName() const
{
    return name;
}

void JsonParser::read(const QJsonObject &json)
{
    name = json["name"].toString();
    work = json["work"].toString();
    id = json["id"].toInt();
}

QJsonObject JsonParser::ObjectFromString(const QString &in)
{
    QJsonObject obj;

    QJsonDocument doc = QJsonDocument::fromJson(in.toUtf8());

        // check validity of the document
        if(!doc.isNull())
        {
            if(doc.isObject())
            {
                obj = doc.object();
            }
            else
            {
                qDebug() << "Document is not an object" << endl;
            }
        }
        else
        {
            qDebug() << "Invalid JSON...\n" << in << endl;
        }

        return obj;
}
