#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <QJsonObject>
#include <QJsonDocument>

#include <QString>
#include <QDateTime>
#include <QTextCodec>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>


#include <iostream>
class JsonParser
{
public:
    JsonParser();
    QString getName() const;
    void read(const QJsonObject &json);
    QJsonObject ObjectFromString(const QString& in);
    int id;
    QString name;
    QString work;
private:

};

#endif // JSONPARSER_H
