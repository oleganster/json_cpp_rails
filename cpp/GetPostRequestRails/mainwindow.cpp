#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    railsUrl = "http://localhost:3000/";
    nameLink = "brcode_names/";
    workLink = "brcode_works/";
    dateLink = "brcode_dates/";
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::dataSent()
{
    ui->debugTextBrowser->append("> sending successful\n");
}

void MainWindow::dataSent(QNetworkReply::NetworkError err)
{
    ui->debugTextBrowser->append(QString("%1").arg(err));
}

void MainWindow::dataSent(const QString &str)
{
    ui->debugTextBrowser->append(str);
}

void MainWindow::on_sendRequestButton_clicked()
{
    sender = new DataSender(railsUrl + dateLink);
    senderThread = new QThread(this);
    senderThread->wait();


    sender->setData(ui->nameLine->text(),ui->workLine->text(),ui->startDateEdit->date().toString(), ui->stopDateEdit->date().toString());



    connect(senderThread, SIGNAL(started()), sender, SLOT(start()));
    connect(sender, SIGNAL(sendingFinished()), senderThread, SLOT(quit()));
    connect(sender, SIGNAL(sendingFinished()), sender, SLOT(deleteLater()));
    connect(sender, SIGNAL(sendingFinished()), this, SLOT(dataSent()));
    connect(senderThread, SIGNAL(finished()), senderThread, SLOT(deleteLater()));
    connect(sender, SIGNAL(sendDataFailure(QString)), this, SLOT(dataSent(QString)));
    connect(sender, SIGNAL(sendDataFailure(QNetworkReply::NetworkError)), this, SLOT(dataSent(QNetworkReply::NetworkError)));
    connect(sender, SIGNAL(sendDataFailure(QNetworkReply::NetworkError)), senderThread, SLOT(quit()));

    ui->debugTextBrowser->append("> sending started");
    senderThread->start();
}

void MainWindow::on_getNameButton_clicked()
{
    QNetworkAccessManager * manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

    QString url = railsUrl + nameLink + ui->nameBarcodeLine->text() + ".json";
    manager->get(QNetworkRequest(QUrl(url)));

    ui->debugTextBrowser->append(tr("> request sent"));
}

void MainWindow::on_getWorkButton_clicked()
{
    QNetworkAccessManager * manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

    QString url = railsUrl + workLink + ui->workBarcodeLine->text() + ".json";
    manager->get(QNetworkRequest(QUrl(url)));

    ui->debugTextBrowser->append(tr("> request sent"));
}

void MainWindow::replyFinished(QNetworkReply * reply)
{
    ui->debugTextBrowser->append(tr("> got reply"));

    if (reply->error() == QNetworkReply::NoError)
    {
        QByteArray bytes = reply->readAll();
        QString string(bytes);
        ui->debugTextBrowser->append("> json from server: " + string + "\n");

        QJsonObject jsonObj = parser.ObjectFromString(string);
        parser.read(jsonObj);
        if (parser.name!=""){
            ui->returnNameLine->setText(parser.name);
            ui->nameLine->setText(QString("%0").arg(parser.id));

        }
        if (parser.work!=""){
            ui->returnWorkLine->setText(parser.work);
            ui->workLine->setText(QString("%0").arg(parser.id));
        }
    }
    else
    {
        //handle errors
    }

    reply->deleteLater();
}


