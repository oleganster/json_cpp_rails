#include "datasender.h"
#include <QDebug>

DataSender::DataSender(const QString &url, QObject *parent) :
    QObject(parent), url(url), manager(0)
{
}

DataSender::~DataSender()
{
    if(manager)
        delete manager;
}

void DataSender::setData(const QString &name, const QString &work, const QString &start_date, const QString &stop_date)
{
    this->name = name;
    this->work = work;
    this->start_date = start_date;
    this->stop_date = stop_date;
}

QHttpMultiPart * DataSender::prepareMultipart()
{
    QHttpMultiPart * multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    multiPart->append(createPart("form-data; name=\"brcode_date[stop_date]\"", this->stop_date));
    multiPart->append(createPart("form-data; name=\"brcode_date[start_date]\"", this->start_date));
    multiPart->append(createPart("form-data; name=\"brcode_date[work]\"", this->work));
    multiPart->append(createPart("form-data; name=\"brcode_date[name]\"", this->name));
    return multiPart;
}

QHttpPart DataSender::createPart(const QString &header, const QString &body)
{
    QHttpPart part;
    part.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(header));
    part.setBody(body.toUtf8());
    return part;
}

void DataSender::start()
{
    manager = new QNetworkAccessManager();
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(httpRequestFinished(QNetworkReply*)), Qt::DirectConnection);
    QNetworkRequest request(url);
    QHttpMultiPart * multipart = prepareMultipart();

    manager->post(request, multipart);

}

void DataSender::httpRequestFinished(QNetworkReply * reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        emit sendingFinished();
    }
    else
    {
        emit sendDataFailure(reply->error());
    }
}
