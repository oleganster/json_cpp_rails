#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QJsonObject>
#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QFileDialog>
#include <QThread>
#include "datasender.h"
#include "jsonparser.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void dataSent();
    void dataSent(QNetworkReply::NetworkError err);
    void dataSent(const QString & str);
private slots:
    void on_sendRequestButton_clicked();
    void replyFinished(QNetworkReply *reply);
    void on_getNameButton_clicked();

    void on_getWorkButton_clicked();

private:
    QString railsUrl;
    QString nameLink;
    QString workLink;
    QString dateLink;
    DataSender * sender;
    JsonParser parser;
    Ui::MainWindow *ui;
    QThread * senderThread;
};

#endif // MAINWINDOW_H
