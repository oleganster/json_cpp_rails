class C < ActiveRecord::Migration[5.0]
  def change
  	change_table :brcode_dates do |t|
  		t.remove :name_id
  		t.remove :work_id
  		t.remove :work_start_date
  		t.remove :work_stop_date
  		t.string :name
  		t.string :work
  		t.string :start_date
  		t.string :stop_date
  	end
  end
end
