class BrcodeWorksController < ApplicationController

	before_action :set_br, only: [:destroy]

	def index
		@brcode_works = BrcodeWork.all
		@brcode_work = BrcodeWork.new()
	end

	def new
		@brcode_work = BrcodeWork.new()
	end

	def create
		@brcode_work = BrcodeWork.new(br_params)
		@brcode_work.save
		redirect_to brcode_works_path
	end

	def destroy
		@brcode_work.destroy
		redirect_to brcode_works_path
	end

	def show
		@brcode_work = BrcodeWork.find_by barcode_id: params[:id]
		respond_to do |format|
	      	format.html
	      	format.json { render :json => @brcode_work }
	      	format.xml  {render :xml => @brcode_work}
      	end
		
	end

	private
	def br_params
		params.require(:brcode_work).permit(
			:barcode_id,
			:work)
		
	end

	def set_br
		@brcode_work = BrcodeWork.find(params[:id])
	end
end
