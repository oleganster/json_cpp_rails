class BrcodeDatesController < ApplicationController
	before_action :set_br, only: [:destroy]
	skip_before_action :verify_authenticity_token

	def index
		@brcode_dates = BrcodeDate.all
		@brcode_date = BrcodeDate.new()
	end

	def new
		@brcode_date = BrcodeDate.new()
	end

	def create
		@brcode_date = BrcodeDate.new(br_params)
		@brcode_date.save
		redirect_to brcode_dates_path
	end

	def destroy
		@brcode_date.destroy
		redirect_to brcode_dates_path
	end

	private
	def br_params
		params.require(:brcode_date).permit(
			:name,
			:work,
			:start_date,
			:stop_date)
		
	end

	def set_br
		@brcode_date = BrcodeDate.find(params[:id])
	end
end
