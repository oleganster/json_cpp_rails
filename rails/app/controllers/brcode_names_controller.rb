class BrcodeNamesController < ApplicationController

	before_action :set_br, only: [:destroy]

	def index
		@brcode_names = BrcodeName.all
		@brcode_name = BrcodeName.new()
	end

	def new
		@brcode_name = BrcodeName.new()
	end

	def create
		@brcode_name = BrcodeName.new(br_params)
		@brcode_name.save
		redirect_to brcode_names_path
	end

	def destroy
		@brcode_name.destroy
		redirect_to brcode_names_path
	end

	def show
		@brcode_name = BrcodeName.find_by barcode_id: params[:id]
		respond_to do |format|
	      	format.html
	      	format.json { render :json => @brcode_name }
	      	format.xml  {render :xml => @brcode_name}
      	end
	end

	private
	def br_params
		params.require(:brcode_name).permit(
			:barcode_id,
			:name)
		
	end

	def set_br
		@brcode_name = BrcodeName.find(params[:id])
	end
	
end
