Rails.application.routes.draw do
	resources :brcode_names
	resources :brcode_works
	resources :brcode_dates
	root "brcode_names#index"
end
